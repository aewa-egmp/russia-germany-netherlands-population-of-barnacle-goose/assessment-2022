##### This starts from July and DOES TAKE into account the second ramdom step
Rprogram <- "03"
source("../../.Rutils/Utils.R")
source("../../.Rutils/print.R")
library(xlsx)

##### Order of posterior chains
posteriorOrder <- 1:5

##### Fixed value for relative sensitivity of Fledglings to harvest around January
phi <- 1

##### Define different harvest Rates as percentages
pRate.BS.s <- c(0, 0.75)
pRate.BS.w <- c(0, 0.75)
pRate.NS.s <- c(0, 0.75)
pRate.NS.w <- c(0, 1.25)

pRate.B.s <- c(0, 0.5)
pRate.B.w <- c(0, 1.5)
pRate.N.s <- c(0, 20)
pRate.N.w <- c(0, 15)

##### Standard errors for second step in random effects model
##### Mode=1 does take the second step in the random effects model into account
mode <- 1
sigma.Survival <- 4
sigma.Repro    <- 2
sd.Survival    <- sigma.Survival/2
sd.Repro       <- sigma.Repro/2

##### Percentiles to calculate (symmetric)
cperc <- c(2.5, 5, 10, 25)
cperc <- c(2.5, 5, 25)
cperc <- sort(c(cperc, 100-cperc))

##### Make harvest rates of equal length by recycling and Combine rates
pRate <- data.frame(B.s=pRate.B.s, B.w=pRate.B.w, N.s=pRate.N.s, N.w=pRate.N.w,
    BS.s=pRate.BS.s, BS.w=pRate.BS.w, NS.s=pRate.NS.s, NS.w=pRate.NS.w)
hRate <- pRate/100
nScenario <- nrow(hRate)
pr(hRate)

##### Load posteriors for initial population size / survival / reproduction
load(".RData/01-GetPosterior.RData")
nposterior <- length(posterior)
nposterior
if (nposterior != max(posteriorOrder)) stop("")

##### Transform muLogit.SURV and mulogit.REPRO to survival/reproduction scale
cnames <- colnames(posterior[[1]])
colSurvival <- cnames[which(startsWith(cnames, "muLogit.s"))]
colRepro    <- cnames[which(startsWith(cnames, "muLogit.r"))]
colOther    <- cnames[!(cnames %in% c(colSurvival,colRepro))]
for (ii in 1:nposterior) {
  if (mode == 1) {
    ##### Second step of random effects model
    valueOther   <- unlist(posterior[[ii]][,colOther])
    meanSurvival <- unlist(posterior[[ii]][,colSurvival])
    ranSurvival  <- rnorm(length(meanSurvival), meanSurvival, sd.Survival)
    meanRepro    <- unlist(posterior[[ii]][,colRepro])
    ranRepro     <- rnorm(length(meanRepro), meanRepro, sd.Repro)
    posterior[[ii]] <- as.data.frame(matrix(c(valueOther,ranSurvival,ranRepro),
        nrow=nrow(posterior[[ii]]), ncol=ncol(posterior[[ii]])))
    colnames(posterior[[ii]]) <- c(colOther, colSurvival, colRepro)
    posterior[[ii]] <- posterior[[ii]][,cnames]
  }
  ##### Transform to survival/reproduction scale
  posterior[[ii]][,colSurvival] <- 1/(1+exp(-posterior[[ii]][,colSurvival]))
  posterior[[ii]][,colRepro]    <- 2/(1+exp(-posterior[[ii]][,colRepro]))
}

##### Initial population sizes
ii <- posteriorOrder[1]
nRF  <- posterior[[ii]][,"nRF"]
nRA  <- posterior[[ii]][,"nRA"]
nR   <- nRF + nRA
nRBP <- posterior[[ii]][,"nRBP"]
nBF  <- posterior[[ii]][,"nBF"]
nBA  <- posterior[[ii]][,"nBA"]
nB   <- nBF + nBA
nBBP <- posterior[[ii]][,"nBBP"]
nNF  <- posterior[[ii]][,"nNF"]
nNA  <- posterior[[ii]][,"nNA"]
nN   <- nNF + nNA
nNBP <- posterior[[ii]][,"nNBP"]

##### Add extra columns such that "initial" has the same columns as "population"
minOne <- -1 + 0*nRF
initial <- data.frame(
    january=minOne, januaryR=minOne, januaryB=minOne, januaryN=minOne,
    nRF,nRA,nRBP,nR, nBF,nBA,nBBP,nB, nNF,nNA,nNBP,nN,
    harvest.R=minOne,harvest.R1=minOne,harvest.R2=minOne,harvest.R3=minOne,harvest.R4=minOne,
    harvest.B=minOne,harvest.B1=minOne,harvest.B2=minOne,harvest.B3=minOne,harvest.B4=minOne,
    harvest.N=minOne,harvest.N1=minOne,harvest.N2=minOne,harvest.N3=minOne,harvest.N4=minOne,
    harvest.BS=minOne, harvest.BS.s=minOne, harvest.BS.w=minOne,
    harvest.NS=minOne, harvest.NS.s=minOne, harvest.NS.w=minOne)
mean   <- colMeans(initial)
median <- apply(initial, 2, median)
sd     <- apply(initial, 2, sd)
quan   <- apply(initial, 2, quantile, probs=cperc/100)
start  <- as.data.frame(cbind(mean, median, sd, t(quan)))
pr(start)

##### Combine all summaries into a single dataframe
sum <- t(start)

##### Run the population model for each row of hRate
population <- list()
summary    <- list()
for (jj in 1:nScenario) {
  ##### Initialize
  population[[jj]] <- list()
  summary[[jj]] <- list()

  nRF <- initial$nRF
  nRA <- initial$nRA
  nBF <- initial$nBF
  nBA <- initial$nBA
  nNF <- initial$nNF
  nNA <- initial$nNA

  hRate.B.s <- hRate[jj,"B.s"]
  hRate.B.w <- hRate[jj,"B.w"]
  hRate.N.s <- hRate[jj,"N.s"]
  hRate.N.w <- hRate[jj,"N.w"]

  hRate.BS.s <- hRate[jj,"BS.s"]
  hRate.BS.w <- hRate[jj,"BS.w"]
  hRate.NS.s <- hRate[jj,"NS.s"]
  hRate.NS.w <- hRate[jj,"NS.w"]

  hRate.Jan.s <- hRate.BS.s + hRate.NS.s
  hRate.Jan.w <- hRate.BS.w + hRate.NS.w

  ##### Combine summaries into a single dataframe
  if (jj>1) {
    sum <- rbind(sum, t(start))
  }

  for (ii in posteriorOrder) {
    ##### Natural Summer survival in this step
      theta.RF.s <- posterior[[ii]][,"muLogit.sRF.s"]
      theta.RA.s <- posterior[[ii]][,"muLogit.sRA.s"]
      theta.BF.s <- posterior[[ii]][,"muLogit.sBF.s"]
      theta.BA.s <- posterior[[ii]][,"muLogit.sBA.s"]
      theta.NF.s <- posterior[[ii]][,"muLogit.sNF.s"]
      theta.NA.s <- posterior[[ii]][,"muLogit.sNA.s"]
    ##### Natural Winter survival in this step
      theta.RF.w <- posterior[[ii]][,"muLogit.sRF.w"]
      theta.RA.w <- posterior[[ii]][,"muLogit.sRA.w"]
      theta.BF.w <- posterior[[ii]][,"muLogit.sBF.w"]
      theta.BA.w <- posterior[[ii]][,"muLogit.sBA.w"]
      theta.NF.w <- posterior[[ii]][,"muLogit.sNF.w"]
      theta.NA.w <- posterior[[ii]][,"muLogit.sNA.w"]
    ##### Reproduction in this step
      reproR <- posterior[[ii]][,"muLogit.reproR"]
      reproB <- posterior[[ii]][,"muLogit.reproB"]
      reproN <- posterior[[ii]][,"muLogit.reproN"]

    ##### Combined lambda survival (copy/paste from model and deleting [t])
    ## Russian (harvest of local breeding population is included in 'natural' survival)
      lambda.RF.s <- (1-phi*hRate.Jan.s) * theta.RF.s
      lambda.RF.w <- (1-phi*hRate.Jan.w) * theta.RF.w
      lambda.RA.s <- (1-    hRate.Jan.s) * theta.RA.s
      lambda.RA.w <- (1-    hRate.Jan.w) * theta.RA.w
    ## BalticSea (including harvest of local breeding population)
      lambda.BF.s <- (1-hRate.B.s) * (1-phi*hRate.Jan.s) * theta.BF.s
      lambda.BF.w <- (1-hRate.B.w) * (1-phi*hRate.Jan.w) * theta.BF.w
      lambda.BA.s <- (1-hRate.B.s) * (1-    hRate.Jan.s) * theta.BA.s
      lambda.BA.w <- (1-hRate.B.w) * (1-    hRate.Jan.w) * theta.BA.w
    ## NorthSea (including harvest of local breeding population)
      lambda.NF.s <- (1-hRate.N.s) * (1-phi*hRate.NS.s) * theta.NF.s
      lambda.NF.w <- (1-hRate.N.w) * (1-phi*hRate.NS.w) * theta.NF.w
      lambda.NA.s <- (1-hRate.N.s) * (1-    hRate.NS.s) * theta.NA.s
      lambda.NA.w <- (1-hRate.N.w) * (1-    hRate.NS.w) * theta.NA.w

    ## Harvest in the local breeding population in Summer (just after July)
      harvest.R1 <- 0*nR
      harvest.B1 <- hRate.B.s * nB
      harvest.N1 <- hRate.N.s * nN

    ## Harvest around January in Summer/Winter
    ## Summer harvest just before January employing population 'size' which is prone to harvest
    ## Note that theta is natural survival
      sizeR.s <-                 (phi*theta.RF.s*nRF + theta.RA.s*nRA)
      sizeB.s <- (1-hRate.B.s) * (phi*theta.BF.s*nBF + theta.BA.s*nBA)
      sizeN.s <- (1-hRate.N.s) * (phi*theta.NF.s*nNF + theta.NA.s*nNA)
      harvest.R2 <- (hRate.BS.s + hRate.NS.s) * sizeR.s
      harvest.B2 <- (hRate.BS.s + hRate.NS.s) * sizeB.s
      harvest.N2 <- hRate.NS.s * sizeN.s
      harvest.BS.s <- hRate.BS.s * (sizeR.s + sizeB.s)
      harvest.NS.s <- hRate.NS.s * (sizeR.s + sizeB.s  + sizeN.s)
    ## Winter harvest just after January employing population 'size' which is prone to harvest
    ## Note that lambda is combined harvest and natural survival
      sizeR.w <- phi*lambda.RF.s*nRF + lambda.RA.s*nRA
      sizeB.w <- phi*lambda.BF.s*nBF + lambda.BA.s*nBA
      sizeN.w <- phi*lambda.NF.s*nNF + lambda.NA.s*nNA
      harvest.R3 <- (hRate.BS.w + hRate.NS.w) * sizeR.w
      harvest.B3 <- (hRate.BS.w + hRate.NS.w) * sizeB.w
      harvest.N3 <- hRate.NS.w * sizeN.w
      harvest.BS.w <- hRate.BS.w * (sizeR.w + sizeB.w)
      harvest.NS.w <- hRate.NS.w * (sizeR.w + sizeB.w  + sizeN.w)
    ## Harvest in the local breeding populations at the end of the winter (just before July)
    ## lambda.BA.w and lambda.BF.w both include (1-hRate.B.w[t]) and therefore the correction below is required
    ## Note that this offtake is AFTER reproduction
      sizeB <- lambda.BA.s*lambda.BA.w*nBA * (1 + 0.5*reproB) + lambda.BF.s*lambda.BF.w*nBF
      sizeN <- lambda.NA.s*lambda.NA.w*nNA * (1 + 0.5*reproN) + lambda.NF.s*lambda.NF.w*nNF
      harvest.R4 <- 0*nR
      harvest.B4 <- (hRate.B.w/(1-hRate.B.w)) * sizeB
      harvest.N4 <- (hRate.N.w/(1-hRate.N.w)) * sizeN

    ## Combine harvest
      harvest.R <- harvest.R1 + harvest.R2 + harvest.R3 + harvest.R4
      harvest.B <- harvest.B1 + harvest.B2 + harvest.B3 + harvest.B4
      harvest.N <- harvest.N1 + harvest.N2 + harvest.N3 + harvest.N4
      harvest.BS <- harvest.BS.s + harvest.BS.w
      harvest.NS <- harvest.NS.s + harvest.NS.w

    ##### January (copy/paste from model and deleting [t])
      januaryR <- lambda.RF.s*nRF + lambda.RA.s*nRA
      januaryB <- lambda.BF.s*nBF + lambda.BA.s*nBA
      januaryN <- lambda.NF.s*nNF + lambda.NA.s*nNA
      january  <- januaryR + januaryB + januaryN
    ##### July (copy/paste from model and deleting [t] and [t+1])
    ##### Do not change the order of the equations!
      nRBP <- 0.5*lambda.RA.s*lambda.RA.w*nRA
      nRA  <- lambda.RA.s*lambda.RA.w*nRA + lambda.RF.s*lambda.RF.w*nRF
      nRF  <- reproR*nRBP
      nR   <- nRF + nRA
      nBBP <- 0.5*lambda.BA.s*lambda.BA.w*nBA
      nBA  <- lambda.BA.s*lambda.BA.w*nBA + lambda.BF.s*lambda.BF.w*nBF
      nBF  <- reproB*nBBP
      nB   <- nBF + nBA
      nNBP <- 0.5*lambda.NA.s*lambda.NA.w*nNA
      nNA  <- lambda.NA.s*lambda.NA.w*nNA + lambda.NF.s*lambda.NF.w*nNF
      nNF  <- reproN*nNBP
      nN   <- nNF + nNA
    ##### Combine and Summary
      population[[jj]][[ii]] <- data.frame(january,januaryR,januaryB,januaryN,
          nRF,nRA,nRBP,nR, nBF,nBA,nBBP,nB, nNF,nNA,nNBP,nN,
          harvest.R,harvest.R1,harvest.R2,harvest.R3,harvest.R4,
          harvest.B,harvest.B1,harvest.B2,harvest.B3,harvest.B4,
          harvest.N,harvest.N1,harvest.N2,harvest.N3,harvest.N4,
          harvest.BS, harvest.BS.s, harvest.BS.w,
          harvest.NS, harvest.NS.s, harvest.NS.w)
      mean   <- colMeans(population[[jj]][[ii]])
      median <- apply(population[[jj]][[ii]], 2, median)
      sd     <- apply(population[[jj]][[ii]], 2, sd)
      quan   <- apply(population[[jj]][[ii]], 2, quantile, probs=cperc/100)
      summary[[jj]][[ii]] <- as.data.frame(cbind(mean, median, sd, t(quan)))
      cat(paste("Scenario", jj, " ->  Year",ii,"\n"))
      pr(summary[[jj]][[ii]])
    ##### Combine summaries into a single dataframe
      sum <- rbind(sum, t(summary[[jj]][[ii]]))
  }
}
##### Add extra columns to sum for identification of rows
what     <- rep(colnames(start), (nposterior+1)*nScenario)
scenario <- rep(1:nScenario, each=nrow(sum)/nScenario)
year     <- rep(rep(0:nposterior, each=nrow(sum)/nScenario/(nposterior+1)), nScenario)
sum      <- cbind(what,scenario,year,sum)
pr(sum[,1:6], deci=0)
save(pRate,hRate,population,summary,sum, file=paste0(".RData/", Rprogram, "-Scenario.Rdata"))
sumFile   <- paste0(".RData/", Rprogram, "-Scenario-Summary.csv")
write.csv(sum,   file=sumFile, row.names=FALSE, quote=FALSE)

##### Save to Excel; also save Median for all scenario's
sum <- read.csv(sumFile)
xlsx <- paste0(".RData/", Rprogram, "-Scenario.xlsx")
append <- FALSE
write.xlsx(pRate, file=xlsx, append=append, sheetName="Rates", showNA=FALSE)
append <- TRUE
write.xlsx(sum, file=xlsx, append=append, sheetName="Summary", showNA=FALSE)
for (jj in 1:2) {
  sumMedian <- sum[(sum$what == "median") & (sum$scenario == jj),]
  sumMedian <- t(sumMedian[,4:ncol(sum)])
  sumMedian <- round(sumMedian)
  sumMedian[sumMedian<=0] <- NA
  colnames(sumMedian) <- paste0("Year-", 1:ncol(sumMedian)-1)
  pr(sumMedian, deci=0, mis='-')
  write.xlsx(sumMedian, file=xlsx, append=append, sheetName=paste0(jj,"-Median"), showNA=FALSE)
}
q()

## Employing data from the season 2005/06 onwards
## Relative sensitivity of Fledglings to harvest phi = 1.0
## Beta-binomial distribution for percentage Fledglings
Rprogram = "04-Model"

##### Priors for process error of Counts/Harvest
intervalCVcounts  <- c(2, 5)      # January count Russian + BalticSea + NorthSea
intervalCVcountsB <- c(2, 10)     # July count BalticSea
intervalCVcountsN <- c(2, 10)     # July count NorthSea
intervalCVharvest <- c(5, 40)     # Harvest
coverageProb <- 0.95
## Fixed value for relative sensitivity of Fledglings to harvest around January
phi <- 1.0

options(width=120)
library("xlsx")
source("../.Rutils/gamma.parms.from.quantiles.R")
source("../.Rutils/Utils.R")
source("../.Rutils/print.R")
seed   <- 843832 + as.numeric(substr(Rprogram, 0, 2))
set.seed(seed)

## Starting year of IPM
firstYearIPM <- 2006

## Read data
data <- read.xlsx("../modelinput_compiled_2022_v3.xlsx", sheetName="2022")
data <- data[data$Year >= firstYearIPM,]
data$Nr <- data$Nr - min(data$Nr) + 1
pr(data[,c("Nr","Year","Count","CountB","CountN")])

## Harvest <= 16 are replaced by missing; Note that harvest can be non-integer
limitHarvest <- 16
dataNames <- colnames(data)
getHarvest <- dataNames[which(startsWith(dataNames, "h"))]
getHarvest
for (hh in getHarvest) {
  condition <- !(is.na(data[,hh])) & (data[,hh] <= limitHarvest)
  data[condition, hh] <- NA
}
pr(data[,c("Nr","Year",getHarvest)], deci=0, mis='-')

## First years of harvest BS/NS when migratory birds are present
firstYearSummerHarvestBS <- min(data[!is.na(data$hSummerBS), "Year"])
firstYearWinterHarvestBS <- min(data[!is.na(data$hWinterBS), "Year"])
firstYearSummerHarvestNS <- min(data[!is.na(data$hSummerNS), "Year"])
firstYearWinterHarvestNS <- min(data[!is.na(data$hWinterNS), "Year"])

## First years of harvest local population B/N
firstYearLocalSummerHarvestB <- min(data[!is.na(data$hSummerB), "Year"])
firstYearLocalWinterHarvestB <- min(data[!is.na(data$hWinterB), "Year"])
firstYearLocalSummerHarvestN <- min(data[!is.na(data$hSummerN), "Year"])
firstYearLocalWinterHarvestN <- min(data[!is.na(data$hWinterN), "Year"])

##### First years that data are available for Fledglings proportions in the local populations
firstYearFledglingsB <- min(data[!is.na(data$nFledB), "Year"])
firstYearFledglingsN <- min(data[!is.na(data$nFledN), "Year"])

## Russian initial population size at t=1
## Initial population size is for July preceeding the first January count (44225)
## The observed January count is therefore increased with 5%
initPopR <- 1.05*data$Count[1]
initPopB <- data$CountB[1]
initPopN <- data$CountN[1]
initPopR <- initPopR - initPopB - initPopN

## Check Data firstYears of Harvest
pr(data[data$Year %in% (firstYearSummerHarvestBS+c(-2:2)), c("Year","Season","hSummerBS")], row=F, mis='-', deci=0)
pr(data[data$Year %in% (firstYearWinterHarvestBS+c(-2:2)), c("Year","Season","hWinterBS")], row=F, mis='-', deci=0)
pr(data[data$Year %in% (firstYearSummerHarvestNS+c(-2:2)), c("Year","Season","hSummerNS")], row=F, mis='-', deci=0)
pr(data[data$Year %in% (firstYearWinterHarvestNS+c(-2:2)), c("Year","Season","hWinterNS")], row=F, mis='-', deci=0)
pr(data[data$Year %in% (firstYearLocalSummerHarvestB+  c(-2:2)), c("Year","Season","hSummerB","hWinterB")], row=F, mis='-', deci=0)
pr(data[data$Year %in% (firstYearLocalSummerHarvestN+  c(-2:2)), c("Year","Season","hSummerN","hWinterN")], row=F, mis='-', deci=0)

## set the array indices for the populations
T  <- nrow(data)

## set the array indices for BS/NS harvest around January
TstartSummerHarvestBS <- data[data$Year==firstYearSummerHarvestBS, "Nr"]
TstartWinterHarvestBS <- data[data$Year==firstYearWinterHarvestBS, "Nr"]
TstartSummerHarvestNS <- data[data$Year==firstYearSummerHarvestNS, "Nr"]
TstartWinterHarvestNS <- data[data$Year==firstYearWinterHarvestNS, "Nr"]
pr(cbind(TstartSummerHarvestBS,TstartWinterHarvestBS, TstartSummerHarvestNS,TstartWinterHarvestNS), row=F)

##### Set the array indices for the local harvest in BalticSea and NorthSea
TstartLocalSummerHarvestB  <- data[data$Year==firstYearLocalSummerHarvestB, "Nr"]
TstartLocalWinterHarvestB  <- data[data$Year==firstYearLocalWinterHarvestB, "Nr"]
TstartLocalSummerHarvestN  <- data[data$Year==firstYearLocalSummerHarvestN, "Nr"]
TstartLocalWinterHarvestN  <- data[data$Year==firstYearLocalWinterHarvestN, "Nr"]
pr(cbind(TstartLocalSummerHarvestB,TstartLocalWinterHarvestB,TstartLocalSummerHarvestN,TstartLocalWinterHarvestN), row=F)

##### Set the array indices for Local Fledglings proportions
TstartFledgB <- data[data$Year==firstYearFledglingsB, "Nr"]
TstartFledgN <- data[data$Year==firstYearFledglingsN, "Nr"]
pr(cbind(TstartFledgB,TstartFledgN), row=F)

## Initial population sizes
## It is assumed that 15% of the population are Fledglings
percFledglings <- 15
{
  ## Russian
  initPopRF  <- initPopR * percFledglings/100
  initPopRA  <- initPopR * (100-percFledglings)/100
  muInit.nRF <- log(initPopRF)
  muInit.nRA <- log(initPopRA)
  ## BalticSea; first reported count
  initPopBF  <- initPopB * percFledglings/100
  initPopBA  <- initPopB * (100-percFledglings)/100
  muInit.nBF <- log(initPopBF)
  muInit.nBA <- log(initPopBA)
  ## NorthSea; first reported count
  initPopNF  <- initPopN * percFledglings/100
  initPopNA  <- initPopN * (100-percFledglings)/100
  muInit.nNF <- log(initPopNF)
  muInit.nNA <- log(initPopNA)
}

##### 95% range of initial Fledglings/Adults numbers assuming a certain CV
## CV value of initial population size is 50%/20% for Fledglings/Adults
## Note that JAGS::dlnorm employs 1/Variance while R::dlnorm employs the Variance
cvF <- 50
cvA <- 20
tauInit.nF  <- 1/log((cvF/100)^2 + 1)   # Inverse of lognormal variance (precision)
tauInit.nA  <- 1/log((cvA/100)^2 + 1)   # Inverse of lognormal variance (precision)
initPop <- c(initPopR, initPopB, initPopN)
Fledg   <- c(initPopRF, initPopBF, initPopNF)
FledgLow <- qlnorm(0.025, log(Fledg) - 0.5/tauInit.nF, sqrt(1/tauInit.nF))
FledgUpp <- qlnorm(0.975, log(Fledg) - 0.5/tauInit.nF, sqrt(1/tauInit.nF))
Adult   <- c(initPopRA, initPopBA, initPopNA)
AdultLow <- qlnorm(0.025, log(Adult) - 0.5/tauInit.nA, sqrt(1/tauInit.nA))
AdultUpp <- qlnorm(0.975, log(Adult) - 0.5/tauInit.nA, sqrt(1/tauInit.nA))
pr(data.frame(Region=c("Russian","Baltic","North"),initPop,Fledg,FledgLow,FledgUpp,Adult,AdultLow,AdultUpp),
    field=c(8,8,11,9,9,11,9,9), row=F)

## Prior parameters for survival of Adults (A), Fledglings (F) in Summer (.s) and Winter (.w)
## Logistic-Normal prior
mu.sF.s <- 0.41;  tau.sF.s <- 4;  sigma.sF.s <- 4
mu.sF.w <- 0.41;  tau.sF.w <- 4;  sigma.sF.w <- 4
mu.sA.s <- 2.94;  tau.sA.s <- 4;  sigma.sA.s <- 4
mu.sA.w <- 2.94;  tau.sA.w <- 4;  sigma.sA.w <- 4

## Prior parameters for Reproduction: Logistic-Normal multiplied by 2
mu.repro <- 0;  tau.repro <- 2;  sigma.repro <- 2

## Prior parameters for proportion Fledglings January Flyway and BalticSea/NorthSea local populations
## Estimates based on Years >= 2005
shapeFledgling  <- 1*2.6635;  rateFledgling  <- 0.059374
shapeFledglingB <- 5*2.6635;  rateFledglingB <- 0.059374
shapeFledglingN <- 1*2.6635;  rateFledglingN <- 0.059374

## Prior parameters for uniform harvest Rates
hRateLowerBS <- 0.00;  hRateUpperBS <- 0.10
hRateLowerNS <- 0.00;  hRateUpperNS <- 0.10
hRateLowerB  <- 0.00;  hRateUpperB  <- 0.10
hRateLowerN  <- 0.00;  hRateUpperN  <- 0.30

## Process error for January counts: LogNormal.
## The %CV value is with probability coverageProb in intervalCVcounts
quantiles <- c((1-coverageProb)/2, (1+coverageProb)/2)
parms <- gamma.parms.from.quantiles(intervalCVcounts/100, quantiles)
shapeCount <- parms$shape
rateCount <- 1/parms$scale
pr(cbind(shapeCount, rateCount), row=F)

## Check that shape and rate are correct
cvCount <- rgamma(100000, shape=shapeCount, rate=rateCount)
meanCV <- 100*mean(cvCount)
qqCV <- quantile(100*cvCount, quantiles)
mean <- 100*shapeCount/rateCount
q1 <- 100*qgamma(quantiles[1], shape=shapeCount, rate=rateCount)
q2 <- 100*qgamma(quantiles[2], shape=shapeCount, rate=rateCount)
pr(cbind(meanCV, qq1=qqCV[1], qq2=qqCV[2], mean, q1, q2), row=F)

## Process error for July counts of local BalticSea populations: LogNormal.
parms <- gamma.parms.from.quantiles(intervalCVcountsB/100, quantiles)
shapeCountB <- parms$shape
rateCountB  <- 1/parms$scale

## Process error for July counts of local NorthSea populations: LogNormal.
parms <- gamma.parms.from.quantiles(intervalCVcountsN/100, quantiles)
shapeCountN <- parms$shape
rateCountN  <- 1/parms$scale

## Process error for Harvest counts: LogNormal.
parms <- gamma.parms.from.quantiles(intervalCVharvest/100, quantiles)
shapeHarvest <- parms$shape
rateHarvest <- 1/parms$scale

## Define list with structures to pass to JAGS
small <- 1.0e-8
JAGSinput <- list(
  ## Indices for population
    T=T, T.min=T - 1,
  ## Indices for Harvest BalticSea Region
    TstartSummerHarvestBS=TstartSummerHarvestBS, TstartSummerHarvestBS.min=TstartSummerHarvestBS - 1,
    TstartWinterHarvestBS=TstartWinterHarvestBS, TstartWinterHarvestBS.min=TstartWinterHarvestBS - 1,
  ## Indices for Harvest NorhtSea Region
    TstartSummerHarvestNS=TstartSummerHarvestNS, TstartSummerHarvestNS.min=TstartSummerHarvestNS - 1,
    TstartWinterHarvestNS=TstartWinterHarvestNS, TstartWinterHarvestNS.min=TstartWinterHarvestNS - 1,
  ## Indices for Harvest local population BalticSea and NorthSea
    TstartLocalSummerHarvestB=TstartLocalSummerHarvestB, TstartLocalSummerHarvestB.min=TstartLocalSummerHarvestB - 1,
    TstartLocalWinterHarvestB=TstartLocalWinterHarvestB, TstartLocalWinterHarvestB.min=TstartLocalWinterHarvestB - 1,
    TstartLocalSummerHarvestN=TstartLocalSummerHarvestN, TstartLocalSummerHarvestN.min=TstartLocalSummerHarvestN - 1,
    TstartLocalWinterHarvestN=TstartLocalWinterHarvestN, TstartLocalWinterHarvestN.min=TstartLocalWinterHarvestN - 1,
  ## Indices for Fledlings Data BalticSea and NorthSea populations
    TstartFledgB=TstartFledgB,
    TstartFledgN=TstartFledgN,
  ## Observed population counts
    obsCount.Jan=data$Count, obsCount.B=data$CountB, obsCount.N=data$CountN,
  ## Observed harvest numbers around January, when migratory birds are present
    obsSummerHarvestBS=data$hSummerBS, obsWinterHarvestBS=data$hWinterBS,
	  obsSummerHarvestNS=data$hSummerNS, obsWinterHarvestNS=data$hWinterNS,
  ## Observed harvest numbers around July, when birds are at their breeding site
    obsLocalSummerHarvestB=data$hSummerB, obsLocalWinterHarvestB=data$hWinterB,
	  obsLocalSummerHarvestN=data$hSummerN, obsLocalWinterHarvestN=data$hWinterN,
  ## Observed number of Fledglings in group sizes in January for R+B
    obsFledglings=data$nFledgling, obsGroupSize=data$nGroup,
  ## Observed number of Fledglings in group sizes in July for BalticSea and for NorthSea
    obsFledglingsB=data$nFledB, obsGroupSizeB=data$nGroupB,
    obsFledglingsN=data$nFledN, obsGroupSizeN=data$nGroupN,
  ## prior paramaters for initial population sizes
    muInit.nRF=muInit.nRF, muInit.nRA=muInit.nRA,       # Russian
    muInit.nBF=muInit.nBF, muInit.nBA=muInit.nBA,       # BalticSea
    muInit.nNF=muInit.nNF, muInit.nNA=muInit.nNA,       # NorthSea
    tauInit.nF=tauInit.nF, tauInit.nA=tauInit.nA,       # precision
  ## prior parameters for natural survival rates; common prior for R/B/N
    mu.sF.s=mu.sF.s, tau.sF.s=tau.sF.s, sigma.sF.s=sigma.sF.s,
    mu.sF.w=mu.sF.w, tau.sF.w=tau.sF.w, sigma.sF.w=sigma.sF.w,
    mu.sA.s=mu.sA.s, tau.sA.s=tau.sA.s, sigma.sA.s=sigma.sA.s,
    mu.sA.w=mu.sA.w, tau.sA.w=tau.sA.w, sigma.sA.w=sigma.sA.w,
  ## prior parameters for reproduction; common prior for R/B/N
    mu.repro=mu.repro, tau.repro=tau.repro, sigma.repro=sigma.repro,
  ## prior parameters for beta-binomial for number of Fledglings
    shapeFledgling= shapeFledgling,  rateFledgling= rateFledgling,
    shapeFledglingB=shapeFledglingB, rateFledglingB=rateFledglingB,
    shapeFledglingN=shapeFledglingN, rateFledglingN=rateFledglingN,
  ## prior for process error of Counts and Harvest
    shapeCount= shapeCount,  rateCount= rateCount,
    shapeCountB=shapeCountB, rateCountB=rateCountB,
    shapeCountN=shapeCountN, rateCountN=rateCountN,
    shapeHarvest=shapeHarvest, rateHarvest=rateHarvest,
  ## prior for Harvest rates
    hRateLowerBS=hRateLowerBS, hRateUpperBS=hRateUpperBS,
    hRateLowerNS=hRateLowerNS, hRateUpperNS=hRateUpperNS,
    hRateLowerB= hRateLowerB,  hRateUpperB= hRateUpperB,
    hRateLowerN= hRateLowerN,  hRateUpperN= hRateUpperN,
  ## phi:   relative sensitivity to harvest of Fledglings vs Adults
  ## small: used to prevent division by zero
    phi=phi, small=small)
save(JAGSinput, file=paste0(Rprogram, "-Input.RData"))
JAGSinput

## Define the JAGS model
## Make sure that all vectors are of equal length
JAGSmodel <- "model
{
  ##### PRIORS FOR INITIAL POPULATION SIZE
  ## Prior for Russian initial population size in July, just after reproduction
  ## For initial number of Breeding Pairs see Model equation for population update below
    nRF[1] ~ dlnorm(muInit.nRF, tauInit.nF)    # Russian Fledglings
    nRA[1] ~ dlnorm(muInit.nRA, tauInit.nA)    # Russian Adults
    nRBP[1] <- nRF[1]/reproR[1]                # Russian Breeding Pairs; few months before July

  ## Prior for BalticSea initial population size in July, just after reproduction
    nBF[1] ~ dlnorm(muInit.nBF, tauInit.nF)        # BalticSea Fledglings
    nBA[1] ~ dlnorm(muInit.nBA, tauInit.nA)        # BalticSea Adults
    nBBP[1] <- nBF[1]/reproR[1]                    # BalticSea Breeding Pairs; few months before July

  ## Prior for NorthSea initial population size in July, just after reproduction
    nNF[1] ~ dlnorm(muInit.nNF, tauInit.nF)        # NorthSea Fledglings
    nNA[1] ~ dlnorm(muInit.nNA, tauInit.nA)        # NorthSea Adults
    nNBP[1] <- nNF[1]/reproR[1]                    # NorthSea Breeding Pairs; few months before July

  ##### HYPER-PRIORS AND PRIORS
  ## Hyper-prior Logit-Normal for Survival Fledgling/Adult in Summer/Winter
  ## Russian
    muLogit.sRF.s ~ dnorm(mu.sF.s, tau.sF.s)
    muLogit.sRF.w ~ dnorm(mu.sF.w, tau.sF.w)
    muLogit.sRA.s ~ dnorm(mu.sA.s, tau.sA.s)
    muLogit.sRA.w ~ dnorm(mu.sA.w, tau.sA.w)
  ## BalticSea
    muLogit.sBF.s ~ dnorm(mu.sF.s, tau.sF.s)
    muLogit.sBF.w ~ dnorm(mu.sF.w, tau.sF.w)
    muLogit.sBA.s ~ dnorm(mu.sA.s, tau.sA.s)
    muLogit.sBA.w ~ dnorm(mu.sA.w, tau.sA.w)
  ## NorthSea
    muLogit.sNF.s ~ dnorm(mu.sF.s, tau.sF.s)
    muLogit.sNF.w ~ dnorm(mu.sF.w, tau.sF.w)
    muLogit.sNA.s ~ dnorm(mu.sA.s, tau.sA.s)
    muLogit.sNA.w ~ dnorm(mu.sA.w, tau.sA.w)

  ## Hyper-prior Logit-Normal for Reproduction
  ## Separately for Russian, BalticSea, NorthSea
    muLogit.reproR ~ dnorm(mu.repro, tau.repro)
    muLogit.reproB ~ dnorm(mu.repro, tau.repro)
    muLogit.reproN ~ dnorm(mu.repro, tau.repro)

  ## Prior for Beta-Binomial dispersion for proportion of Fledglings January Count
  ## January proportion is assumed to be for Russian + BalticSea population, so excluding NorthSea population
    dispFledgling ~ dgamma(shapeFledgling, rateFledgling)

  ## Prior for Beta-Binomial dispersion for proportion of Fledglings BalticSea and NorthSea population in July
    dispFledglingB ~ dgamma(shapeFledglingB, rateFledglingB)
    dispFledglingN ~ dgamma(shapeFledglingN, rateFledglingN)

  ## Prior for process error for January Counts of R+B+N
  ## Prior for process error for July Counts of BalticSea and NorthSea
    cvCount ~ dgamma(shapeCount, rateCount)         ## January Count of Flyway
    log.tauCount <- 1/log(cvCount*cvCount + 1)
    cvCountB ~ dgamma(shapeCountB, rateCountB)      ## July Count BalticSea population
    log.tauCountB <- 1/log(cvCountB*cvCountB + 1)
    cvCountN ~ dgamma(shapeCountN, rateCountN)      ## July Count NorthSea population
    log.tauCountN <- 1/log(cvCountN*cvCountN + 1)

  ## Prior for process error for Harvest Counts around January when migratory birds are present
  ## Separate for BalticSea region (BS) and for NorthSea region (NS) in Winter/Summer
    cvHarvest.Jan.BS ~ dgamma(shapeHarvest, rateHarvest)
    cvHarvest.Jan.NS ~ dgamma(shapeHarvest, rateHarvest)
    log.tauHarvest.Jan.BS <- 1/log(cvHarvest.Jan.BS*cvHarvest.Jan.BS + 1)
    log.tauHarvest.Jan.NS <- 1/log(cvHarvest.Jan.NS*cvHarvest.Jan.NS + 1)

  ## Prior for process error for Harvest around July when birds are at their local breeding site
  ## Separate for BalticSea population (B) and for NorthSea population (N)
    cvHarvest.Jul.B ~ dgamma(shapeHarvest, rateHarvest)
    cvHarvest.Jul.N ~ dgamma(shapeHarvest, rateHarvest)
    log.tauHarvest.Jul.B <- 1/log(cvHarvest.Jul.B*cvHarvest.Jul.B + 1)
    log.tauHarvest.Jul.N <- 1/log(cvHarvest.Jul.N*cvHarvest.Jul.N + 1)

  ##### MODEL EQUATIONS FOR PARAMETERS
  ## PARAMETERS: Natural survival for Fledglings/Adults in Summer/Winter; separately for the three populations
  for (t in 1:T) {
    ## Russian
      logit.theta.RF.s[t] ~ dnorm(muLogit.sRF.s, 4/(sigma.sF.s*sigma.sF.s))   # Fledgling summer
      logit.theta.RF.w[t] ~ dnorm(muLogit.sRF.w, 4/(sigma.sF.w*sigma.sF.w))   # Fledgling winter
      logit.theta.RA.s[t] ~ dnorm(muLogit.sRA.s, 4/(sigma.sA.s*sigma.sA.s))   # Adult summer
      logit.theta.RA.w[t] ~ dnorm(muLogit.sRA.w, 4/(sigma.sA.w*sigma.sA.w))   # Adult winter
    ## BalticSea
      logit.theta.BF.s[t] ~ dnorm(muLogit.sBF.s, 4/(sigma.sF.s*sigma.sF.s))   # Fledgling summer
      logit.theta.BF.w[t] ~ dnorm(muLogit.sBF.w, 4/(sigma.sF.w*sigma.sF.w))   # Fledgling winter
      logit.theta.BA.s[t] ~ dnorm(muLogit.sBA.s, 4/(sigma.sA.s*sigma.sA.s))   # Adult summer
      logit.theta.BA.w[t] ~ dnorm(muLogit.sBA.w, 4/(sigma.sA.w*sigma.sA.w))   # Adult winter
    ## NorthSea
      logit.theta.NF.s[t] ~ dnorm(muLogit.sNF.s, 4/(sigma.sF.s*sigma.sF.s))   # Fledgling summer
      logit.theta.NF.w[t] ~ dnorm(muLogit.sNF.w, 4/(sigma.sF.w*sigma.sF.w))   # Fledgling winter
      logit.theta.NA.s[t] ~ dnorm(muLogit.sNA.s, 4/(sigma.sA.s*sigma.sA.s))   # Adult summer
      logit.theta.NA.w[t] ~ dnorm(muLogit.sNA.w, 4/(sigma.sA.w*sigma.sA.w))   # Adult winter
    ## Transform to probability scale
      logit(theta.RF.s[t]) <- logit.theta.RF.s[t]
      logit(theta.RF.w[t]) <- logit.theta.RF.w[t]
      logit(theta.RA.s[t]) <- logit.theta.RA.s[t]
      logit(theta.RA.w[t]) <- logit.theta.RA.w[t]
      logit(theta.BF.s[t]) <- logit.theta.BF.s[t]
      logit(theta.BF.w[t]) <- logit.theta.BF.w[t]
      logit(theta.BA.s[t]) <- logit.theta.BA.s[t]
      logit(theta.BA.w[t]) <- logit.theta.BA.w[t]
      logit(theta.NF.s[t]) <- logit.theta.NF.s[t]
      logit(theta.NF.w[t]) <- logit.theta.NF.w[t]
      logit(theta.NA.s[t]) <- logit.theta.NA.s[t]
      logit(theta.NA.w[t]) <- logit.theta.NA.w[t]
  }

  ## PARAMETERS: Harvest rates around JANUARY in BalticSea and NorthSea Region (BS/NS) when migratory birds are present
    ## Years WITHOUT Harvest around JANUARY
    for (t in 1:TstartSummerHarvestBS.min) {  ## Summer Harvest BS
      hRate.BS.s[t] <- 0
    }
    for (t in 1:TstartWinterHarvestBS.min) {  ## Winter Harvest BS
      hRate.BS.w[t] <- 0
    }
    for (t in 1:TstartSummerHarvestNS.min) {  ## Summer Harvest NS
      hRate.NS.s[t] <- 0
    }
    for (t in 1:TstartWinterHarvestNS.min) {  ## Winter Harvest NS
      hRate.NS.w[t] <- 0
    }
    ## Years WITH harvest around JANUARY
    for (t in TstartSummerHarvestBS:T) {  ## Summer Harvest BS
      hRate.BS.s[t] ~ dunif(hRateLowerBS, hRateUpperBS)
    }
    for (t in TstartWinterHarvestBS:T) {  ## Winter Harvest BS
      hRate.BS.w[t] ~ dunif(hRateLowerBS, hRateUpperBS)
    }
    for (t in TstartSummerHarvestNS:T) {  ## Summer Harvest NS
      hRate.NS.s[t] ~ dunif(hRateLowerNS, hRateUpperNS)
    }
    for (t in TstartWinterHarvestNS:T) {  ## Winter Harvest NS
      hRate.NS.w[t] ~ dunif(hRateLowerNS, hRateUpperNS)
    }

  ## PARAMETERS: Combined harvest rates around JANUARY for BalticSea and NorthSea Regions (BS/NS) for all years
    for (t in 1:T) {
      hRate.Jan.s[t] <- hRate.BS.s[t] + hRate.NS.s[t]
      hRate.Jan.w[t] <- hRate.BS.w[t] + hRate.NS.w[t]
    }

  ## PARAMETERS: Harvest rates around JULY in BalticSea/NortSea, affecting only the local breeding populations
    ## Years WITHOUT harvest around JULY
    for (t in 1:TstartLocalSummerHarvestB.min) { ## Summer Harvest B
      hRate.B.s[t] <- 0
    }
    for (t in 1:TstartLocalWinterHarvestB.min) { ## Winter Harvest B
      hRate.B.w[t] <- 0
    }
    for (t in 1:TstartLocalSummerHarvestN.min) { ## Summer Harvest N
      hRate.N.s[t] <- 0
    }
    for (t in 1:TstartLocalWinterHarvestN.min) { ## Winter Harvest N
      hRate.N.w[t] <- 0
    }
    ## Years WITH harvest around JULY
    for (t in TstartLocalSummerHarvestB:T) {  ## Summer Harvest B
      hRate.B.s[t] ~ dunif(hRateLowerB, hRateUpperB)
    }
    for (t in TstartLocalWinterHarvestB:T) {  ## Winter Harvest B
      hRate.B.w[t] ~ dunif(hRateLowerB, hRateUpperB)
    }
    for (t in TstartLocalSummerHarvestN:T) {  ## Summer Harvest N
      hRate.N.s[t] ~ dunif(hRateLowerN, hRateUpperN)
    }
    for (t in TstartLocalWinterHarvestN:T) {  ## Winter Harvest N
      hRate.N.w[t] ~ dunif(hRateLowerN, hRateUpperN)
    }

  ## PARAMETERS: Combined harvest and survival
  ## Russian and BalticSea population are subjected to harvest in BalticSea and NorthSea --> hRate.Jan
  ## NorthSea population is only subjected to harvest in NorthSea --> hRate.NS
  for (t in 1:T) {
    ## Russian (harvest of local breeding population is included in 'natural' survival)
      lambda.RF.s[t] <- (1-phi*hRate.Jan.s[t]) * theta.RF.s[t]
      lambda.RF.w[t] <- (1-phi*hRate.Jan.w[t]) * theta.RF.w[t]
      lambda.RA.s[t] <- (1-    hRate.Jan.s[t]) * theta.RA.s[t]
      lambda.RA.w[t] <- (1-    hRate.Jan.w[t]) * theta.RA.w[t]
    ## BalticSea (including harvest of local breeding population)
      lambda.BF.s[t] <- (1-hRate.B.s[t]) * (1-phi*hRate.Jan.s[t]) * theta.BF.s[t]
      lambda.BF.w[t] <- (1-hRate.B.w[t]) * (1-phi*hRate.Jan.w[t]) * theta.BF.w[t]
      lambda.BA.s[t] <- (1-hRate.B.s[t]) * (1-    hRate.Jan.s[t]) * theta.BA.s[t]
      lambda.BA.w[t] <- (1-hRate.B.w[t]) * (1-    hRate.Jan.w[t]) * theta.BA.w[t]
    ## NorthSea (including harvest of local breeding population)
      lambda.NF.s[t] <- (1-hRate.N.s[t]) * (1-phi*hRate.NS.s[t]) * theta.NF.s[t]
      lambda.NF.w[t] <- (1-hRate.N.w[t]) * (1-phi*hRate.NS.w[t]) * theta.NF.w[t]
      lambda.NA.s[t] <- (1-hRate.N.s[t]) * (1-    hRate.NS.s[t]) * theta.NA.s[t]
      lambda.NA.w[t] <- (1-hRate.N.w[t]) * (1-    hRate.NS.w[t]) * theta.NA.w[t]
  }

  ## PARAMETERS: Reproduction
  for (t in 1:T) {
    ## Reproduction on the logit scale
      logit.reproR[t] ~ dnorm(muLogit.reproR, 4/(sigma.repro*sigma.repro))
      logit.reproB[t] ~ dnorm(muLogit.reproB, 4/(sigma.repro*sigma.repro))
      logit.reproN[t] ~ dnorm(muLogit.reproN, 4/(sigma.repro*sigma.repro))
    ## Transform to probability scale
      reproR[t] <- 2/(1+exp(-logit.reproR[t]))
      reproB[t] <- 2/(1+exp(-logit.reproB[t]))
      reproN[t] <- 2/(1+exp(-logit.reproN[t]))
  }

  ##### MODEL EQUATIONS FOR COUNTS AND HARVEST
  ## MODEL: July population dynamics for Russian population
  ## Breeding pairs are those that survive winter breeding population offtake
  for (t in 1:T.min) {
    nRBP[t+1] <- 0.5*lambda.RA.s[t]*lambda.RA.w[t]*nRA[t]
    nRA[t+1]  <- lambda.RA.s[t]*lambda.RA.w[t]*nRA[t] + lambda.RF.s[t]*lambda.RF.w[t]*nRF[t]
    nRF[t+1]  <- reproR[t]*nRBP[t+1]
  }
  ## MODEL: July population dynamics for BalticSea population
  for (t in 1:T.min) {
    nBBP[t+1] <- 0.5*lambda.BA.s[t]*lambda.BA.w[t]*nBA[t]
    nBA[t+1]  <- lambda.BA.s[t]*lambda.BA.w[t]*nBA[t] + lambda.BF.s[t]*lambda.BF.w[t]*nBF[t]
    nBF[t+1]  <- reproB[t]*nBBP[t+1]
  }
  ## MODEL: July population dynamics for NorthSea population
  for (t in 1:T.min) {
    nNBP[t+1] <- 0.5*lambda.NA.s[t]*lambda.NA.w[t]*nNA[t]
    nNA[t+1]  <- lambda.NA.s[t]*lambda.NA.w[t]*nNA[t] + lambda.NF.s[t]*lambda.NF.w[t]*nNF[t]
    nNF[t+1]  <- reproN[t]*nNBP[t+1]
  }

  ## MODEL: population totals in July for monitoring; also monitoring of percentage Juveniles
  ## Includes 'small' to prevent division by zero
  for (t in 1:T) {
    nR[t] <- nRA[t] + nRF[t]
    nB[t] <- nBA[t] + nBF[t]
    nN[t] <- nNA[t] + nNF[t]
    julyPop[t] <- nR[t] + nB[t] + nN[t]
    percRF[t] <- 100*nRF[t]/(nR[t] + small)
    percBF[t] <- 100*nBF[t]/(nB[t] + small)
    percNF[t] <- 100*nNF[t]/(nN[t] + small)
  }

  ## MODEL: population totals in January;
  ## Note that the January count is after the July count in the same season. For example in the
  ## season 2005/2006 the 26070/21000 counts in the North/Baltic Sea are in July 2005
  ## while the corresponding flyway count of 693423 is in January 2006.
  for (t in 1:T) {
    januaryRF[t] <- lambda.RF.s[t]*nRF[t]
    januaryRA[t] <- lambda.RA.s[t]*nRA[t]
    januaryR[t]  <- januaryRF[t] + januaryRA[t]
    januaryBF[t] <- lambda.BF.s[t]*nBF[t]
    januaryBA[t] <- lambda.BA.s[t]*nBA[t]
    januaryB[t]  <- januaryBF[t] + januaryBA[t]
    januaryNF[t] <- lambda.NF.s[t]*nNF[t]
    januaryNA[t] <- lambda.NA.s[t]*nNA[t]
    januaryN[t]  <- januaryNF[t] + januaryNA[t]
    januaryPop[t]   <- januaryR[t] + januaryB[t] + januaryN[t]
    januaryFledg[t] <- januaryRF[t] + januaryBF[t] + januaryNF[t]
    januaryAdult[t] <- januaryRA[t] + januaryBA[t] + januaryNA[t]
  }

  ## MODEL: Harvest of the local breeding populations at the start of the summer (just after July)
  ## Note that harvest rates are zero before 'firstYearHarvest'
  for (t in 1:T) {
    harvest.B.s[t] <- hRate.B.s[t] * nB[t]   ## BalticSea
    harvest.N.s[t] <- hRate.N.s[t] * nN[t]   ## NorthSea
  }

  ## MODEL: Harvest around January in Summer/Winter
  ## Note that harvest rates are zero before 'firstYearHarvest'
  for (t in 1:T) {
    ## Summer harvest just before January employing population 'size' which is prone to harvest
    ## Note that theta is natural survival
      sizeR.s[t] <-                    (phi*theta.RF.s[t]*nRF[t] + theta.RA.s[t]*nRA[t])
      sizeB.s[t] <- (1-hRate.B.s[t]) * (phi*theta.BF.s[t]*nBF[t] + theta.BA.s[t]*nBA[t])
      sizeN.s[t] <- (1-hRate.N.s[t]) * (phi*theta.NF.s[t]*nNF[t] + theta.NA.s[t]*nNA[t])
      harvest.BS.s[t] <- hRate.BS.s[t] * (sizeR.s[t] + sizeB.s[t])
      harvest.NS.s[t] <- hRate.NS.s[t] * (sizeR.s[t] + sizeB.s[t] + sizeN.s[t])
    ## Winter harvest just after January employing population 'size' which is prone to harvest
    ## Note that lambda is combined harvest and natural survival
      sizeR.w[t] <- phi*lambda.RF.s[t]*nRF[t] + lambda.RA.s[t]*nRA[t]
      sizeB.w[t] <- phi*lambda.BF.s[t]*nBF[t] + lambda.BA.s[t]*nBA[t]
      sizeN.w[t] <- phi*lambda.NF.s[t]*nNF[t] + lambda.NA.s[t]*nNA[t]
      harvest.BS.w[t] <- hRate.BS.w[t] * (sizeR.w[t] + sizeB.w[t])
      harvest.NS.w[t] <- hRate.NS.w[t] * (sizeR.w[t] + sizeB.w[t] + sizeN.w[t])
  }

  ## MODEL: Harvest of the local breeding populations at the end of the winter (just before July)
  ## Note that harvest rates are zero before 'firstYearHarvest'
  ## lambda.BA.w and lambda.BF.w both include (1-hRate.B.w[t]) and therefore the correction below is required
  for (t in 1:T) {  ## BalticSea
    ## Note that this offtake is AFTER reproduction
    ## Note that sizeB[t] equals nBA[t+1] + nBF[t+1] = nB[t+1]
    sizeB[t] <- lambda.BA.s[t]*lambda.BA.w[t]*nBA[t] * (1 + 0.5*reproB[t]) + lambda.BF.s[t]*lambda.BF.w[t]*nBF[t]
    harvest.B.w[t] <- (hRate.B.w[t]/(1-hRate.B.w[t])) * sizeB[t]
  }
  for (t in 1:T) {  ## NorthSea
    ## Note that this offtake is AFTER reproduction
    ## Note that sizeN[t] equals nNA[t+1] + nNF[t+1] = nN[t+1]
    sizeN[t] <- lambda.NA.s[t]*lambda.NA.w[t]*nNA[t] * (1 + 0.5*reproN[t]) + lambda.NF.s[t]*lambda.NF.w[t]*nNF[t]
    harvest.N.w[t] <- (hRate.N.w[t]/(1-hRate.N.w[t])) * sizeN[t]
  }

  ## MODEL: Monitoring yearly natural survival for each population
  for (t in 1:T) {  ## NorthSea
    theta.RF[t] <- theta.RF.s[t] * theta.RF.w[t]   ## Including (unkwown) harvest at the breeding site
    theta.RA[t] <- theta.RA.s[t] * theta.RA.w[t]
    theta.BF[t] <- theta.BF.s[t] * theta.BF.w[t]   ## Excluding harvest
    theta.BA[t] <- theta.BA.s[t] * theta.BA.w[t]
    theta.NF[t] <- theta.NF.s[t] * theta.NF.w[t]   ## Excluding harvest
    theta.NA[t] <- theta.NA.s[t] * theta.NA.w[t]
  }

## MODEL: Monitoring yearly harvest rate for each population
  for (t in 1:T) {
    ## Adults
    tmpA[t] <- (1-(hRate.BS.s[t]+hRate.NS.s[t])) * (1-(hRate.BS.w[t]+hRate.NS.w[t]))
    hRate.RA[t] <- 1 - tmpA[t]
    hRate.BA[t] <- 1 - (1-hRate.B.s[t]) * tmpA[t] * (1-hRate.B.w[t])
    hRate.NA[t] <- 1 - (1-hRate.N.s[t]) * (1-hRate.NS.s[t]) * (1-hRate.NS.w[t]) * (1-hRate.N.w[t])
    ## Fledglings
    tmpF[t] <- (1-phi*(hRate.BS.s[t]+hRate.NS.s[t])) * (1-phi*(hRate.BS.w[t]+hRate.NS.w[t]))
    hRate.RF[t] <- 1 - tmpF[t]
    hRate.BF[t] <- 1 - (1-hRate.B.s[t]) * tmpF[t] * (1-hRate.B.w[t])
    hRate.NF[t] <- 1 - (1-hRate.N.s[t]) * (1-phi*hRate.NS.s[t]) * (1-phi*hRate.NS.w[t]) * (1-hRate.N.w[t])
  }

  ##### DATA VERSUS MODEL
  ## DATA: Counts in July for BalticSea
  for (t in 1:T) {
    log.julyBalticSea[t] <- log(nBA[t] + nBF[t])
    obsCount.B[t] ~ dlnorm(log.julyBalticSea[t], log.tauCountB)
  }

  ## DATA: Counts in July for NorthSea
  for (t in 1:T) {
    log.julyNorthSea[t]  <- log(nNA[t] + nNF[t])
    obsCount.N[t] ~ dlnorm(log.julyNorthSea[t],  log.tauCountN)
  }

  ## DATA: Counts in January for Russian + BalticSea + NorthSea
  for (t in 1:T) {
    log.januaryPop[t] <- log(januaryPop[t])
    obsCount.Jan[t] ~ dlnorm(log.januaryPop[t], log.tauCount)
  }

  ## DATA: Proportion of fledglings in January; assumed to be Russian+BalticSea population only
  for (t in 1:T) {
    pFledgling[t] <- (lambda.RF.s[t]*nRF[t] + lambda.BF.s[t]*nBF[t])/(januaryR[t] + januaryB[t])
    betaFledgling[t] ~ dbeta(dispFledgling*pFledgling[t], dispFledgling*(1-pFledgling[t]))
    obsFledglings[t] ~ dbin(betaFledgling[t], obsGroupSize[t])      ## Beta-Binomial
    ## obsFledglings[t] ~ dbin(pFledgling[t], obsGroupSize[t])	    ## Binomial
  }

  ## DATA: Proportion of fledglings in July for BalticSea and NorthSea populations
  for (t in 1:T) {
    pFledglingB[t] <- nBF[t]/(nBF[t] + nBA[t])
    betaFledglingB[t] ~ dbeta(dispFledglingB*pFledglingB[t], dispFledglingB*(1-pFledglingB[t]))
  }
  for (t in TstartFledgB:T) {
    obsFledglingsB[t] ~ dbin(betaFledglingB[t], obsGroupSizeB[t])	## Beta-Binomial
    ## obsFledglingsB[t] ~ dbin(pFledglingB[t], obsGroupSizeB[t])   ## Binomial
  }
  for (t in 1:T) {
    pFledglingN[t] <- nNF[t]/(nNF[t] + nNA[t])
    betaFledglingN[t] ~ dbeta(dispFledglingN*pFledglingN[t], dispFledglingN*(1-pFledglingN[t]))
  }
  for (t in TstartFledgN:T) {
    obsFledglingsN[t] ~ dbin(betaFledglingN[t], obsGroupSizeN[t])	## Beta-Binomial
    ## obsFledglingsN[t] ~ dbin(pFledglingN[t], obsGroupSizeN[t])	## Binomial
  }

  ## DATA: Harvest around January in BalticSea/NorthSea regions in Summer/Winter for specific Years
    for (t in TstartSummerHarvestBS:T) {  ## Summer Harvest BS
      log.harvest.BS.s[t] <- log(harvest.BS.s[t])
      obsSummerHarvestBS[t]  ~ dlnorm(log.harvest.BS.s[t], log.tauHarvest.Jan.BS)
    }
    for (t in TstartWinterHarvestBS:T) {  ## Winter Harvest BS
      log.harvest.BS.w[t] <- log(harvest.BS.w[t])
      obsWinterHarvestBS[t]  ~ dlnorm(log.harvest.BS.w[t], log.tauHarvest.Jan.BS)
    }
    for (t in TstartSummerHarvestNS:T) {  ## Summer Harvest NS
      log.harvest.NS.s[t] <- log(harvest.NS.s[t])
      obsSummerHarvestNS[t]  ~ dlnorm(log.harvest.NS.s[t], log.tauHarvest.Jan.NS)
    }
    for (t in TstartWinterHarvestNS:T) {  ## Winter Harvest NS
      log.harvest.NS.w[t] <- log(harvest.NS.w[t])
      obsWinterHarvestNS[t]  ~ dlnorm(log.harvest.NS.w[t], log.tauHarvest.Jan.NS)
    }

  ## DATA: Harvest around July in local BalticSea/NorthSea populations in Summer/Winter for specific Years
    for (t in TstartLocalSummerHarvestB:T) {  ## Summer Harvest B
      log.harvest.B.s[t] <- log(harvest.B.s[t])
      obsLocalSummerHarvestB[t]  ~ dlnorm(log.harvest.B.s[t], log.tauHarvest.Jul.B)
    }
    for (t in TstartLocalWinterHarvestB:T) {  ## Winter Harvest B
      log.harvest.B.w[t] <- log(harvest.B.w[t])
      obsLocalWinterHarvestB[t]  ~ dlnorm(log.harvest.B.w[t], log.tauHarvest.Jul.B)
    }
    for (t in TstartLocalSummerHarvestN:T) {  ## Summer Harvest N
      log.harvest.N.s[t] <- log(harvest.N.s[t])
      obsLocalSummerHarvestN[t]  ~ dlnorm(log.harvest.N.s[t], log.tauHarvest.Jul.N)
    }
    for (t in TstartLocalWinterHarvestN:T) {  ## Winter Harvest N
      log.harvest.N.w[t] <- log(harvest.N.w[t])
      obsLocalWinterHarvestN[t]  ~ dlnorm(log.harvest.N.w[t], log.tauHarvest.Jul.N)
    }
}"

## Load JAGS libraries
library(coda)
library(rjags)
library(runjags)

## What to monitor
JAGSmonitor <- c("januaryPop", "januaryFledg", "januaryAdult",
      "januaryRF", "januaryRA", "januaryR",
      "januaryBF", "januaryBA", "januaryB",
      "januaryNF", "januaryNA", "januaryN",
    "julyPop", "nR","nRF","nRA","nRBP", "nB","nBF","nBA","nBBP", "nN","nNF","nNA","nNBP",
    "percRF", "percBF", "percNF",
    "pFledgling", "betaFledgling",
    "pFledglingB", "betaFledglingB",
    "pFledglingN", "betaFledglingN",
    "theta.RF.s", "theta.RF.w", "theta.RA.s", "theta.RA.w", "theta.RF", "theta.RA",
    "theta.BF.s", "theta.BF.w", "theta.BA.s", "theta.BA.w", "theta.BF", "theta.BA",
    "theta.NF.s", "theta.NF.w", "theta.NA.s", "theta.NA.w", "theta.NF", "theta.NA",
    "reproR", "reproB", "reproN",
    "hRate.BS.s", "hRate.BS.w", "hRate.NS.s", "hRate.NS.w",
    "harvest.BS.s", "harvest.BS.w", "harvest.NS.s", "harvest.NS.w",
    "hRate.B.s", "hRate.B.w", "hRate.N.s", "hRate.N.w",
    "harvest.B.s", "harvest.B.w", "harvest.N.s", "harvest.N.w",
    "hRate.RF", "hRate.BF", "hRate.NF",
    "hRate.RA", "hRate.BA", "hRate.NA",
    "muLogit.sRF.s", "muLogit.sRF.w", "muLogit.sRA.s", "muLogit.sRA.w",
    "muLogit.sBF.s", "muLogit.sBF.w", "muLogit.sBA.s", "muLogit.sBA.w",
    "muLogit.sNF.s", "muLogit.sNF.w", "muLogit.sNA.s", "muLogit.sNA.w",
    "muLogit.reproR", "muLogit.reproB", "muLogit.reproN",
    "dispFledgling", "dispFledglingB", "dispFledglingN",
    "cvCount", "cvCountB", "cvCountN",
    "cvHarvest.Jan.BS", "cvHarvest.Jan.NS", "cvHarvest.Jul.B", "cvHarvest.Jul.N")

## MCMC settings
## MCMC settings
test <- TRUE
test <- FALSE
if (test) {
  adapt  <- 200
  burnin <- 1000
  sample <- 2000
  thin   <- 1
  chains <- 2
} else {
  adapt  <- 5000
  burnin <- 10000
  sample <- 50000
  thin   <- 2
  chains <- 5
}

## Set initial values
vNA <- rep(NA,T)
TMPinits <- list(
    nRF=vNA, nRA=vNA, nBF=vNA, nBA=vNA, nNF=vNA, nNA=vNA,
    muLogit.sRF.s=NA, muLogit.sRF.w=NA, muLogit.sRA.s=NA, muLogit.sRA.w=NA,
    muLogit.sBF.s=NA, muLogit.sBF.w=NA, muLogit.sBA.s=NA, muLogit.sBA.w=NA,
    muLogit.sNF.s=NA, muLogit.sNF.w=NA, muLogit.sNA.s=NA, muLogit.sNA.w=NA,
    muLogit.reproR=NA, muLogit.reproB=NA, muLogit.reproN=NA,
    dispFledgling=NA, dispFledglingB=NA, dispFledglingN=NA,
    cvCount=NA, cvCountB=NA, cvCountN=NA,
    cvHarvest.Jan.BS=NA, cvHarvest.Jan.NS=NA, cvHarvest.Jul.B=NA, cvHarvest.Jul.N=NA,
    .RNG.seed=seed, .RNG.name="base::Wichmann-Hill")

# Inits; repeat for different chains
JAGSinits <- list()
initpopCV <- 5
initpopVAR <- log((initpopCV/100)^2 + 1)
initpopSD  <- sqrt(initpopVAR)
meanFledgling <- shapeFledgling/rateFledgling
meanFledgling

c1 <- 0.5
for (ii in 1:chains) {
  TMPinits$nRF[1] <- rlnorm(1, log(initPopRF) - initpopVAR/2, initpopSD)
  TMPinits$nRA[1] <- rlnorm(1, log(initPopRA) - initpopVAR/2, initpopSD)
  TMPinits$nBF[1] <- rlnorm(1, log(initPopBF) - initpopVAR/2, initpopSD)
  TMPinits$nBA[1] <- rlnorm(1, log(initPopBA) - initpopVAR/2, initpopSD)
  TMPinits$nNF[1] <- rlnorm(1, log(initPopNF) - initpopVAR/2, initpopSD)
  TMPinits$nNA[1] <- rlnorm(1, log(initPopNA) - initpopVAR/2, initpopSD)
  TMPinits$muLogit.sRF.s <- runif(1, mu.sF.s - c1, mu.sF.s + c1)
  TMPinits$muLogit.sRF.w <- runif(1, mu.sF.w - c1, mu.sF.w + c1)
  TMPinits$muLogit.sRA.s <- runif(1, mu.sA.s - c1, mu.sA.s + c1)
  TMPinits$muLogit.sRA.w <- runif(1, mu.sA.w - c1, mu.sA.w + c1)
  TMPinits$muLogit.sBF.s <- runif(1, mu.sF.s - c1, mu.sF.s + c1)
  TMPinits$muLogit.sBF.w <- runif(1, mu.sF.w - c1, mu.sF.w + c1)
  TMPinits$muLogit.sBA.s <- runif(1, mu.sA.s - c1, mu.sA.s + c1)
  TMPinits$muLogit.sBA.w <- runif(1, mu.sA.w - c1, mu.sA.w + c1)
  TMPinits$muLogit.sNF.s <- runif(1, mu.sF.s - c1, mu.sF.s + c1)
  TMPinits$muLogit.sNF.w <- runif(1, mu.sF.w - c1, mu.sF.w + c1)
  TMPinits$muLogit.sNA.s <- runif(1, mu.sA.s - c1, mu.sA.s + c1)
  TMPinits$muLogit.sNA.w <- runif(1, mu.sA.w - c1, mu.sA.w + c1)
  TMPinits$muLogit.reproR <- runif(1, mu.repro - c1, mu.repro + c1)
  TMPinits$muLogit.reproB <- runif(1, mu.repro - c1, mu.repro + c1)
  TMPinits$muLogit.reproN <- runif(1, mu.repro - c1, mu.repro + c1)
  TMPinits$dispFledgling <- runif(1, meanFledgling-10, meanFledgling+10)
  TMPinits$dispFledglingB <- runif(1, meanFledgling-10, meanFledgling+10)
  TMPinits$dispFledglingN <- runif(1, meanFledgling-10, meanFledgling+10)
  TMPinits$cvCount  <- rgamma(1, shape=shapeCount,  rate=rateCount)
  TMPinits$cvCountB <- rgamma(1, shape=shapeCountB, rate=rateCountB)
  TMPinits$cvCountN <- rgamma(1, shape=shapeCountN, rate=rateCountN)
  TMPinits$cvHarvest.Jan.BS <- rgamma(1, shape=shapeHarvest, rate=rateHarvest)
  TMPinits$cvHarvest.Jan.NS <- rgamma(1, shape=shapeHarvest, rate=rateHarvest)
  TMPinits$cvHarvest.Jul.B  <- rgamma(1, shape=shapeHarvest, rate=rateHarvest)
  TMPinits$cvHarvest.Jul.N  <- rgamma(1, shape=shapeHarvest, rate=rateHarvest)
  TMPinits$.RNG.seed <- TMPinits$.RNG.seed + 1
  JAGSinits[[ii]] <- TMPinits
}

## Run JAGS model
start_time = Sys.time()
runjags.options(force.summary=TRUE)
#failed.jags(model=JAGSmodel, data=JAGSinput, inits=JAGSinits)
samples <- run.jags(model=JAGSmodel, data=JAGSinput, inits=JAGSinits, monitor=JAGSmonitor,
    n.chains=chains, adapt=adapt, burnin=burnin, sample=sample, thin=thin)
Sys.time()- start_time
save(samples, file=paste0(Rprogram, ".RData"))
q()

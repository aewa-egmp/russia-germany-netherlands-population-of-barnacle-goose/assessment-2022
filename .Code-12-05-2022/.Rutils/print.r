caption <- function(string, underline="=", before=TRUE, after=TRUE) {
  if (before) cat("\n")
  cat(string)
  cat("\n")
  cat(strrep(underline, nchar(string)))
  cat("\n")
  if (after) cat("\n")
}

# This function operates similar to GenStat PRINT
# All structures are printed with at least one space in front, even the first structure when row.names=FALSE
# There is a small issue when printing atomic unnamed vectors. Compare the following
#   zz <- c(1,2,3)
#   pr(zz, field=6, deci=1, row.names=FALSE)
#   pr(c(1,2,3), field=6, deci=1, row.names=FALSE)

# object : dataframe, matrix or vector (numeric, character, factor, logical)
# fieldwidth: Field width in which to print the values of each structure. 
#     A positive value print numbers in F format, while a negative value -n 
#     prints numbers in E-format in width n. Default 1, if necessary the width 
#     is increased to one more character than the longest line
# decimals: Number of decimal places for numerical data structures, default NA
#     uses the setting of significantfigures. For F format the resulting number 
#     of decimals is decreased when the numerical structure can be represented 
#     exactly with fewer decimals. For E format the default value equals 
#     significantfigures
# significantfigures: number of significant figures to use when decimals is 
#     unset (F format) or number of decimals when decimals is unset (E format)
# nspaces: number of spaces to add before each column; default 1
# row.names: whether to print row.names, default NA gives TRUE for data.frame/matrix, FALSE for other types
# missing: What to print for missing numeric values; default "*"
# squash: Whether to omit blank lines in the layout of values (TRUE, FALSE); 
#     default FALSE
# rlwidth: Number of characters to print for row.names; default NA: all

# Note fieldwidth/decimals/significantfigures/nspaces can be set to 
# numeric integer vectors which are repeated if required

pr <- function(object, fieldwidth=1, decimals=NA, significantfigures=4, 
      nspaces=2, row.names=NA, missing="*", squash=FALSE, rlwidth=NA) {

  # print(sys.calls())
  # Argument checking ==============================================
  fault   <- "\n***** Fault in function pr()\n"
  types <- c("data.frame", "matrix", "numeric", "integer", "character", "factor", "logical")
  typesRowNames <- c("data.frame", "matrix")
  debug <- FALSE
  # OBJECT checking
  if (!pr.objectExists(object)) {
    cat(paste0(fault, "Argument object should be {", paste0(types, collapse=", "), "} (object not found).\n\n"))
    stop(.traceback())
  }
  if (!any(class(object) %in% types)) {
    cat(paste0(fault, "Argument object should be {", paste0(types, collapse=", "), "}.\n\n"))
    if (debug) return() else stop(.traceback())
  }
  # FIELDWIDTH checking
  if (!pr.objectExists(fieldwidth)) {
    cat(paste0(fault, "Argument fieldwidth should be set to non-missing integer values (object not found).\n\n"))
    stop(.traceback())
  }
  if (!is.numeric(fieldwidth) | any(fieldwidth != round(fieldwidth)) | any(is.na(fieldwidth)) ) {
    cat(paste0(fault, "Argument fieldwidth should be set to non-missing integer values.\n\n"))
    if (debug) return() else stop(.traceback())
  }
  # DECIMALS checking; note that class(NA) = logical
  if (!pr.objectExists(decimals)) {
    cat(paste0(fault, "Argument decimals should be set to missing or to non-negative integer values (object not found).\n\n"))
    stop(.traceback())
  }
  if ((!class(decimals) %in% c("numeric","logical","integer"))) {
    cat(paste0(fault, "Argument decimals should be set to missing or to non-negative integer values.\n\n"))
    if (debug) return() else stop(.traceback())
  }
  if (any(decimals != round(decimals), na.rm=TRUE) | (any(decimals < 0, na.rm=TRUE))) {
    cat(paste0(fault, "Argument decimals should be set to missing or to non-negative integer values.\n\n"))
    if (debug) return() else stop(.traceback())
  }
  # SIGNIFICANTFIGURES checking
  if (!pr.objectExists(significantfigures)) {
    cat(paste0(fault, "Argument significantfigures should be set to non-missing positive integer values (object not found).\n\n"))
    stop(.traceback())
  }
  if (!is.numeric(significantfigures)) {
    cat(paste0(fault, "Argument significantfigures should be set to non-missing positive integer values.\n\n"))
    if (debug) return() else stop(.traceback())
  }
  if (any(significantfigures != round(significantfigures), na.rm=TRUE) | 
    any(is.na(significantfigures)) | (sum(significantfigures < 1, na.rm=TRUE))) {
    cat(paste0(fault, "Argument significantfigures should be set to non-missing positive integer values.\n\n"))
    if (debug) return() else stop(.traceback())
  }
  # NSPACES checking
  if (!pr.objectExists(nspaces)) {
    cat(paste0(fault, "Argument nspaces should be set to non-missing positive integer values (object not found).\n\n"))
    stop(.traceback())
  }
  if (!is.numeric(nspaces)) {
    cat(paste0(fault, "Argument nspaces should be set to non-missing positive integer values.\n\n"))
    if (debug) return() else stop(.traceback())
  }
  if ((sum(nspaces != round(nspaces), na.rm=TRUE)) | 
    sum(is.na(nspaces)) | (sum(nspaces < 1, na.rm=TRUE))) {
    cat(paste0(fault, "Argument nspaces should be set to non-missing positive integer values.\n\n"))
    if (debug) return() else stop(.traceback())
  }
  # ROW.NAMES checking
  if (!pr.objectExists(row.names)) {
    cat(paste0(fault, "Argument row.names should be set to a single logical or NA (object not found).\n\n"))
    stop(.traceback())
  }
  if (!is.logical(row.names) | (length(row.names) != 1) ) {
    cat(paste0(fault, "Argument row.names should be set to a single logical or NA.\n\n"))
    if (debug) return() else stop(.traceback())
  }
  if (is.na(row.names)) {
      if (any(class(object) %in% typesRowNames)) {
        row.names <- TRUE
      } else {
        row.names <- FALSE
      }
  }
  # MISSING checking
  if (!pr.objectExists(missing)) {
    cat(paste0(fault, "Argument missing  should be set to a single string (object not found).\n\n"))
    stop(.traceback())
  }
  if (!is.character(missing) | (length(missing) != 1) ) {
    cat(paste0(fault, "Argument missing should be set to a single string.\n\n"))
    if (debug) return() else stop(.traceback())
  }
  # SQUASH checking
  if (!pr.objectExists(squash)) {
    cat(paste0(fault, "Argument squash should be set to a single logical (object not found).\n\n"))
    stop(.traceback())
  }
  if (!is.logical(squash) | (length(squash) != 1) ) {
    cat(paste0(fault, "Argument squash should be set to a single logical.\n\n"))
    if (debug) return() else stop(.traceback())
  }
  # RLWIDTH checking; note that class(NA) = logical
  if (!pr.objectExists(rlwidth)) {
    cat(paste0(fault, "Argument rlwidth should be set to missing or to a single positive integer value (object not found).\n\n"))
    stop(.traceback())
  }
  if ((!class(rlwidth) %in% c("numeric","logical","integer"))) {
    cat(paste0(fault, "Argument rlwidth should be set to missing or to a single positive integer value.\n\n"))
    if (debug) return() else stop(.traceback())
  }
  if ((length(rlwidth) != 1) | any(rlwidth != round(rlwidth), na.rm=TRUE) | any(rlwidth <= 0, na.rm=TRUE)) {
    cat(paste0(fault, "Argument rlwidth should be set to missing or to a single positive integer value.\n\n"))
    if (debug) return() else stop(.traceback())
  }

  # End of error checking ==============================================

  # Convert object to dataframe; this simplifies the code because we can loop over columns
  prdf     <- as.data.frame(object)
  ncol     <- ncol(prdf)
  length   <- length(prdf[[1]])
  colNames <- colnames(prdf)
  if (!is.na(rlwidth)) {
    rowNames <- rownames(prdf)
    rlwidth = max(rlwidth, max(nchar(rowNames)))
    rowNames <- paste0(rowNames, paste0(rep(" ", rlwidth), collapse=""))
    rowNames <- substr(rowNames, 1, rlwidth)
    rownames(prdf) <- rowNames
  }

  # For atomic vectors the colName is set to "object". This is repaired in the code below.
  # For unnamed structures colName is set to "" and objectExists=FALSE
  objectExists <- TRUE
  if (any(class(object) %in% types[c(3:6)])) {
    objectName <- deparse(substitute(object))
    objectExists <- exists(objectName, envir=.GlobalEnv)
    if (objectExists) {
      names(prdf) <- objectName
    } else {
      names(prdf) <- c("")
    }
  }

  # Repeat values of field and decimals such that every column has a value
  repField <- rep(fieldwidth, length.out=ncol)
  repDeci  <- rep(decimals,   length.out=ncol)
  repSign  <- rep(significantfigures, length.out=ncol)
  repSpace <- rep(nspaces, length.out=ncol)

  # Take absolute fieldwidth and extend such that there is enough space for the name of the structure
  absField <- abs(repField)
  absField <- pmax(nchar(colNames)+1, absField, na.rm=TRUE)

  # Negative fieldwidth uses scientific E format wih default significantfigures
  cond <- ((repField<0) & (is.na(repDeci)))
  repDeci[cond] <- repSign[cond]
  format <- rep("f", ncol)
  format[repField<0] <- "e"

  # Loop over columns
  subtract <- 1
  for (ii in 1:ncol) {
    classCol <- class(prdf[[ii]])
    # Number of decimals for numeric structures
    if (!(classCol %in% c("character","factor","logical")) & (is.na(repDeci[ii]) | (repDeci[ii]<0))) {
      # NUMERIC
      repDeci[ii] <- pr.decimals(prdf[[ii]], repSign[ii])
    } else if (classCol == "factor") {
      # FACTOR; determine whether textual or numeric levels
      labels <- levels(prdf[[ii]])
      levels <- suppressWarnings(as.numeric(labels))
      numericalLevels <- !Reduce("&", is.na(levels))
      # If numerical levels determine number of decimals
      if ((numericalLevels) & (is.na(repDeci[ii]) | (repDeci[ii]<0))) {
        repDeci[ii] <- pr.decimals(levels, repSign[ii])
      }
    }

    # Print depending on the type
    if (classCol == "character") {
      # CHARACTER: extend the fieldwidth such that there is enough space for the vector
      absField[ii] <- max(max(nchar(prdf[[ii]]))+1, absField[ii], na.rm=TRUE)
      prdf[[ii]] <- formatC(prdf[[ii]], width=absField[ii]-subtract)
    } else if (classCol == "factor") {
      # FACTOR; branch on type of levels
      if (numericalLevels) {
        tmp <- formatC(levels, digits=repDeci[ii], format=format[ii])
        absField[ii] <- max(max(nchar(tmp))+1, absField[ii], na.rm=TRUE)
        tmp <- formatC(levels, digits=repDeci[ii], width=absField[ii]-subtract, format=format[ii])
        tmp <- gsub("NA", missing, tmp)
        prdf[[ii]] <- tmp[prdf[[ii]]]
      } else {
        absField[ii] <- max(max(nchar(labels))+1, absField[ii], na.rm=TRUE)
        prdf[[ii]] <- formatC(as.character(prdf[[ii]]), width=absField[ii]-subtract)
      }
    } else if (classCol == "logical") {
      # LOGICAL: extend the fieldwidth such that there is enough space
      prdf[[ii]] <- as.character(prdf[[ii]])
      absField[ii] <- max(max(nchar(prdf[[ii]]))+1, absField[ii], na.rm=TRUE)
      prdf[[ii]] <- formatC(prdf[[ii]], width=absField[ii]-subtract)
    } else { 
      # NUMERIC
      tmp <- formatC(prdf[[ii]], digits=repDeci[ii], format=format[ii])
      absField[ii] <- max(max(nchar(tmp))+1, absField[ii], na.rm=TRUE)
      prdf[[ii]] <- formatC(prdf[[ii]], digits=repDeci[ii], width=absField[ii]-subtract, format=format[ii])
      prdf[[ii]] <- gsub("NA", missing, prdf[[ii]])
    }
    if (repSpace[ii] > 1) {
      prdf[[ii]] <- paste0(paste0(rep(" ", repSpace[ii]-1), collapse=""), prdf[[ii]])
    }
  }
 
  # PRINTING
  if (!squash) cat("\n")
  if (objectExists) {
    print(prdf, quote=FALSE, row.names=row.names)
  } else {
    # This is used to prevent printing of an extra empty line
    utils::write.table(prdf, quote=FALSE, row.names=row.names, col.names=FALSE)
  }
  if (!squash) cat("\n")
}

pr.decimals <- function(vector, significantfigures=4) {
  # This is a direct translation of GenStat procedure DECIMALS

  # Argument checking ==============================================
  fault   <- "\n***** Fault in function pr()\n"
  # vector checking
  if (!pr.objectExists(vector)) {
    cat(paste0(fault, "Argument vector should be set to a numeric vector (object not found).\n\n"))
    stop(.traceback())
  }
  if (!is.numeric(vector) | !is.vector(vector) ) {
    cat(paste0(fault, "Argument vector should be set to a numeric vector.\n\n"))
    stop(.traceback())
  }
  # significantfigures
  if (!pr.objectExists(significantfigures)) {
    cat(paste0(fault, "argument significantfigures must be set to a single positive integer (object not found).\n\n"))
    stop(.traceback())
  }
  if ( !is.numeric(significantfigures) | (length(significantfigures) != 1) | any(significantfigures < 1) | any(round(significantfigures) != significantfigures) ) {
    cat(paste0(fault, "argument significantfigures must be set to a single positive integer.\n\n"))
    stop(.traceback())
  }
  # End of argument checking

  mean <- mean(abs(vector), na.rm=TRUE)
  if ((mean == 0) || (is.na(mean))) {
    return(0)
  }
  dmax <- floor(log10(max(abs(vector) + .0000001, na.rm=TRUE)))
  if (significantfigures == 1) {
    ndecy <- -(dmax) * (dmax<=0)
  } else {
    sf2 <- max(significantfigures - 2, 0)
  colValues <- 10^(c(0:sf2)-dmax)
  yster <- abs(vector) * matrix(rep(colValues,each=length(vector)), nrow=length(vector), ncol=sf2+1)
  yster <- abs(yster - round(yster))
  maxrest <- apply(yster, 2, max, na.rm=TRUE) > 0.001
  Dd <- dmax - sum(maxrest, na.rm=TRUE)
  ndecy <- -(Dd) * (Dd<=0)
  }
  return(ndecy)  
}

pr.objectExists <- function(x, checknull=TRUE) {
  objectExists <- tryCatch(
    {   # Can be anything for checking as long it returns TRUE
        (is.logical(x) | (TRUE))
    },
    error=function(cond) {
        # Choose a return value in case of error
        return(FALSE)
    },
    warning=function(cond) {
        # Choose a return value in case of warning
        return(FALSE)
    }
  )    
  # Also check for NULL
  if (objectExists && checknull) {
    if (is.null(x)) {
      objectExists=FALSE;
    } else {
      objectExists=TRUE;
    }
  }
  return(objectExists)
}

#' function that shifts vector values to right or left
#'
#' @param x Vector for ehich to shift values
#' @param n Number of places to be shifted.
#'    Positive numbers will shift to the right by default.
#'    Negative numbers will shift to the left by default.
#'    The direction can be inverted by the invert parameter.
#' @param invert Whether or not the default shift directions
#'    should be inverted.
#' @param default The value that should be inserted by default.
#  https://stackoverflow.com/questions/26997586/r-shifting-a-vector
shift <- function(x, n=1, invert=FALSE, default=NA){
  stopifnot(length(x)>=n)
  if(n==0){
    return(x)
  }
  n <- ifelse(invert, n*(-1), n)
  if(n<0){
    n <- abs(n)
    forward=FALSE
  }else{
    forward=TRUE
  }
  if(forward){
    return(c(rep(default, n), x[seq_len(length(x)-n)]))
  }
  if(!forward){
    return(c(x[seq_len(length(x)-n)+n], rep(default, n)))
  }
}

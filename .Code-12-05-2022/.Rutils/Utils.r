## Function to convert mean and sd to lognormal scale; see code Johnson
lognorm.pr <- function(mu,stdev){
  prior.sig <- log(1+(stdev^2/mu^2))
  prior.se <- sqrt(prior.sig)
  prior.est <- log(mu) - 0.5*prior.sig
  result <- c(prior.est,prior.se)
  return(result)
}

## Function to check lognorm.pr, and to also calculate 95% prediction interval
## Note that CV is NOT a percentage but a fraction
lognorm.random <- function(mu, cv, nrandom=100000) {
  result <- lognorm.pr(mu, mu*cv)
  meanlog <- result[1]
  sdlog <- result[2]
  tau <- 1/result[2]^2
  median = exp(meanlog)
  rln <- rlnorm(nrandom, meanlog, sdlog)
  qln <- qlnorm(c(0.025,0.975), meanlog, sdlog)
  rmean <- mean(rln)
  rsd <- sqrt(var(rln))
  rcv <- 100*rsd/rmean
  rmedian <- median(rln)
  values <- c(mu, cv, median, rmean, rsd, rcv, rmedian, qln, meanlog, sdlog, tau)
  names(values) <- c("mu", "cv", "median", "rmean", "rsd", "rcv", "rmedian", "lower95", "upper95", "meanlog", "sdlog", "tau")
  return(values)
}
res <- lognorm.random(1000, 0.1)

## Function to check LogitNormal
logitnorm <- function(mean, sd) {
  rn <- rnorm(100000, mean, sd)
  pp <- 1/(1+exp(-rn))
  Pmean <- mean(pp)
  Psd  <- sqrt(var(pp))
  quan <- 1/(1+exp(-qnorm(c(0.025,0.975), mean, sd)))
  values <- c(mean, sd, Pmean, Psd, quan)
  names(values) <- c("mean", "sd", "Pmean", "Psd", "Plow95", "Pupp95")
  return(values)
}

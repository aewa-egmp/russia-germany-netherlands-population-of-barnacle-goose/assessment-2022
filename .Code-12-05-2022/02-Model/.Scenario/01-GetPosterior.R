library(stringi)
library(coda)
library(rjags)
library(runjags)
source("../../.Rutils/Utils.R")
source("../../.Rutils/print.R")

##### Get last part of directory
library(stringr)
wd <- getwd()
split <- str_split(getwd(), "/")
Rprogram <- split[[1]][length(split[[1]])-1]
RprogramNr <- substr(Rprogram,1,2)
load(paste0("../", Rprogram, ".RData"))

##### Obtain Posteriors for LAST "n" and "january"
cnames <- colnames(samples$mcmc[[1]])
nCount <- cnames[which(startsWith(cnames, "n"))]
nJan   <- cnames[which(startsWith(cnames, "january"))]
nNames <- c(nCount, nJan)
locate <- stri_locate(pattern="[", nNames, fixed=TRUE)[,"start"]
nNames <- unique(substr(nNames, 1, locate-1))
nYears <- cnames[which(startsWith(cnames, "nRF"))]
lastPop <- paste0(nNames, "[", length(nYears), "]")
lastPop
##### and for ALL muLogit parameters
muLogit <- cnames[which(startsWith(cnames, "muLogit"))]
muLogit
getPosterior <- c(lastPop, muLogit)

##### Accumulate posteriors and save
posterior <- list()
for (ii in 1:length(samples$mcmc)) {
  posterior[[ii]] <- as.data.frame(samples$mcmc[[ii]][,getPosterior])
  colnames(posterior[[ii]])[1:length(nNames)] <- nNames
  str(posterior[[ii]])
}
save(posterior, file=".RData/01-GetPosterior.RData")

##### Calculate correlation between posteriors
nposterior <- ncol(posterior[[1]])
corr <- as.data.frame(matrix(nrow=nposterior, ncol=3))
rownames(corr) <- getPosterior
colnames(corr) <- c("corr12","corr13","corr23")
for (ii in 1:nposterior) {
  corr[ii,1] <- cor(posterior[[1]][,ii], posterior[[2]][,ii])
  corr[ii,2]  <- cor(posterior[[1]][,ii], posterior[[3]][,ii])
  corr[ii,3]  <- cor(posterior[[2]][,ii], posterior[[3]][,ii])
}
pr(corr, deci=2)

##### Check that means/medians/sds are equal to summary
##### Note that quantiles are not the same because JAGS calculates HPD intervals
all <- do.call("rbind", posterior)
means <- colMeans(all)
medians <- apply(all, 2, median)
sds  <- apply(all, 2, sd)
quan <- apply(all, 2, quantile, probs=c(0.025,0.975))
sumOwn <- cbind(means, medians, sds, t(quan))
sumOwn

summary <- read.csv(paste0("../", Rprogram, "-Summary.csv"))
summary <- summary[summary$X %in% getPosterior, c(5,3,6,2,4)]
summary

absDiff <- apply(abs(sumOwn-summary), 2, max)
relDiff <- apply(abs((sumOwn-summary)/summary), 2, max)
pr(cbind(absDiff,relDiff), field=-12, deci=2)
q()